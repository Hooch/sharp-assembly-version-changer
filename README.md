# README #

This is readme file.

### Command line arguments ###

```
#!txt


-d, --dir           (Default: .) Base directory to start search for AssemblyInfo.cs files. If ommited working directory is used.

-v, --ver           Required. Version to set in AssemblyInfo files.

-a, --assemblies    List of assembly titles to change version of.

--cav               If set enables change of AssemblyVersion.

--cfv               If set enables change of FileVersion.

--help              Display this help screen.

--version           Display version information.
```