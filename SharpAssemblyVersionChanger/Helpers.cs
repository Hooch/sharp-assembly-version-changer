﻿using System;
using System.IO;
using System.Text;

namespace SharpAssemblyVersionChanger
{
	static class Helpers
	{
		/// <summary>
		/// Determines a text file's encoding by analyzing its byte order mark (BOM).
		/// Defaults to UTF-8 without BOM when detection of the text file's endianness fails.
		/// </summary>
		/// <param name="filename">The text file to analyze.</param>
		/// <returns>The detected encoding.</returns>
		public static Encoding GetEncoding(string filename)
		{
			// Read the BOM
			var bom = new byte[4];

			try
			{
				using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read))
				{
					file.Read(bom, 0, 4);
				}
			}
			catch (IOException)
			{
				return new UTF8Encoding(false);
			}

			// Analyze the BOM
			if (bom[0] == 0x2b && bom[1] == 0x2f && bom[2] == 0x76) return new UTF7Encoding(true);
			if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf) return new UTF8Encoding(true);
			if (bom[0] == 0xff && bom[1] == 0xfe) return new UnicodeEncoding(false, true); //UTF-16LE
			if (bom[0] == 0xfe && bom[1] == 0xff) return new UnicodeEncoding(true, true); //UTF-16BE
			if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff) return new UTF32Encoding(false, true);
			return new UTF8Encoding(false);
		}
	}
}
