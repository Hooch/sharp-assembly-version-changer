﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;

namespace SharpAssemblyVersionChanger
{
	class CommandLineOptions
	{
		[Option('d', "dir", Default = ".", HelpText = "Base directory to start search for AssemblyInfo.cs files. If ommited working directory is used.")]
		public string Directory { get; set; }

		[Option('v', "ver", Required = true, HelpText = "Version to set in AssemblyInfo files.")]
		public Version Version { get; set; }

		[Option('a', "assemblies", Required = false, Separator = ';', HelpText = "List of assembly titles to change version of.")]
		public IEnumerable<string> AssemblyTitles { get; set; }

		[Option("cav", HelpText = "If set enables change of AssemblyVersion.")]
		public bool ChangeAssemblyVersion { get; set; }

		[Option("cfv", HelpText = "If set enables change of FileVersion.")]
		public bool ChangeFileVersion { get; set; }
	}
}
