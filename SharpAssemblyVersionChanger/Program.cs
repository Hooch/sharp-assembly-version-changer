﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CommandLine;

namespace SharpAssemblyVersionChanger
{
	class Program
	{
		private const RegexOptions DefaultRegexOptions = RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase;
		static readonly Regex AssemblyVersionRegex = new Regex(@"^(\s*\[\s*assembly:\s*AssemblyVersion\s*\(\s*"")(?<version>[\d\.*]+)(""\s*\)\s*\])", DefaultRegexOptions);
		static readonly Regex FileVersionRegex = new Regex(@"^(\s*\[\s*assembly:\s*AssemblyFileVersion\s*\(\s*"")(?<version>[\d\.*]+)(""\s*\)\s*\])", DefaultRegexOptions);
		static readonly Regex AssemblyTitleRegex = new Regex(@"^(\s*\[\s*assembly:\s*AssemblyTitle\s*\(\s*"")(?<title>.+?)(""\s*\)\s*\])", DefaultRegexOptions);

		static void Main(string[] args)
		{
			CommandLineOptions options = null;
			Parser parser = new Parser(with =>
			{
				with.CaseSensitive = false;
				with.HelpWriter = Console.Out;
			});

			var parseResults = parser.ParseArguments<CommandLineOptions>(args);
			if (parseResults.Tag == ParserResultType.NotParsed)
			{
				return;
			}
			else
			{
				parseResults.WithParsed(parsed => options = parsed);
			}

			if (options == null)
			{
				return;
			}

			var assemblyInfoFiles = GetAllAssemblyInfoFiles(options.Directory);
			ChangeVersion(assemblyInfoFiles, options.AssemblyTitles, options.Version, options.ChangeAssemblyVersion, options.ChangeFileVersion);
		}

		/// <summary>
		/// Get all AssemblyInfoFiles
		/// </summary>
		/// <param name="baseDirectory">Base search directory.</param>
		/// <returns>List of AssemblyInfo files.</returns>
		static List<string> GetAllAssemblyInfoFiles(string baseDirectory)
		{
			List<string> assemblyInfoFiles = new List<string>();

			if (!Directory.Exists(baseDirectory))
			{
				Console.WriteLine($"Directroy does not exist: {baseDirectory}");
				return assemblyInfoFiles;
			}

			assemblyInfoFiles = Directory.GetFiles(baseDirectory, "AssemblyInfo.cs", SearchOption.AllDirectories).ToList();

			return assemblyInfoFiles;
		}

		/// <summary>
		/// Changes version in files from list that match assemblyNames from other list.
		/// </summary>
		/// <param name="filePaths">All AssemblyInfoPaths</param>
		/// <param name="assemblyNames">Assembly names whose version to change. If null, all will be changed.</param>
		/// <param name="newVersion">New version</param>
		/// <param name="changeAssemblyVersion">If true, AssemblyVersion is changed to <paramref name="newVersion"/>.</param>
		/// <param name="changeFileVersion">If true, FileVersion is changed to <paramref name="newVersion"/>.</param>
		static void ChangeVersion(List<string> filePaths, IEnumerable<string> assemblyNames, Version newVersion, bool changeAssemblyVersion, bool changeFileVersion)
		{
			var assemblyNamesList = assemblyNames as IList<string> ?? assemblyNames.ToList();
			foreach (var filePath in filePaths)
			{
				Console.WriteLine($"Processing file: {filePath}");
				List<string> lines = File.ReadLines(filePath).ToList();
				bool shouldSaveFile = false;

				for (int i = 0; i < lines.Count; i++)
				{
					Match match = AssemblyVersionRegex.Match(lines[i]);

					if (match.Success && changeAssemblyVersion)
					{
						lines[i] = AssemblyVersionRegex.Replace(lines[i], $"${{1}}{newVersion}$2", 1);
						Console.WriteLine($"Old AssemblyVersion: {match.Groups["version"]}");
						continue;
					}

					match = FileVersionRegex.Match(lines[i]);
					if (match.Success && changeFileVersion)
					{
						lines[i] = FileVersionRegex.Replace(lines[i], $"${{1}}{newVersion}$2", 1);
						Console.WriteLine($"Old AssemblyFileVersion: {match.Groups["version"]}");
						continue;
					}

					if (!shouldSaveFile)
					{
						match = AssemblyTitleRegex.Match(lines[i]);
						if (match.Success)
						{
							Console.WriteLine($"Title: {match.Groups["title"]}");
							string assemblyTitle = match.Groups["title"].ToString();
							if (assemblyNamesList.Count == 0 || assemblyNamesList.Any(an => an == assemblyTitle))
							{
								shouldSaveFile = true;
								continue;
							}
						}
					}
				}

				if (shouldSaveFile)
				{
					Encoding fileEncoding = Helpers.GetEncoding(filePath);

					File.WriteAllLines(filePath, lines, fileEncoding);
					Console.WriteLine($"Saving file: {filePath}");
				}

				Console.WriteLine();
			}
		}
	}
}
